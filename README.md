# 4 - Optimiser & Publier une image

## NGINX & PHP-FPM
L'application conteneurisant **NGINX** et **PHP-FPM** se trouve dans le dossier `/nginx-php/`, contenant:

 - le code source de l'application (un unique fichier `index.php`)
 - les fichiers de configuration de **PHP-FPM**, **NGINX** et **Supervisor**
 - le **Dockerfile** nécessaire à la construction de l'image

Démarrer le conteneur:
```sh
docker run --rm -p 8080:80 nginx-php
```
Et accéder à l'application: [http://127.0.0.1:8080](http://127.0.0.1:8080)

>Un fichier `.gitlab-ci.yml` d'exemple est fourni (**template.gitlab-ci.yml**) avec la configuration nécessaire pour construire l'image `nginx-php` à chaque ***push*** dans le projet.

<br>

## NGINX & worker
Pour la partie pratique, les fichiers fournis se trouvent dans le dossier `/nginx-worker/`.

>**Objectif:** construction d'une **image** embarquant un *serveur web* et un *worker* qui écrira régulièrement dans un fichier **HTML**.

L'**image** doit être légère et rapide à construire, vérifier les sujets abordés durant le chapitre.

<br>**1)** Installer:

 - **Bash**
 - **Nginx**
 - **Supervisor**

<br>**2)** Copier les fichiers fournis:

 - **index.html**: l'unique fichier disponible via le serveur web
 - **nginx-server.conf**: fichier de configuration pour *NGINX*
 - **worker.sh**: le script *Bash* du *worker*, qui doit être exécutable

<br>**3)** Créer un fichier de configuration pour **Supervisor** comprenant:

 - 1 programme démarrant **NGINX**
 - 1 programme pour le script *worker.sh*, qui doit être constamment redémarré

<br>**4)** Enfin, créer le dossier `/run/nginx`, exécuter **Supervisor** comme commande par défaut.

<br>**5)** Construire l'image, et définir une stratégie de construction automatique:

 - *hébergé sur GitLab*: **GitLab CI**
 - *hébergé sur GitHub / BitBucket*: **Docker Hub Automated Builds**