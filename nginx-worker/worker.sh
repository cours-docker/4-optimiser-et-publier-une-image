#!/bin/bash

# Ecrire la date courante dans le fichier index.html
echo "$(date) <br>" >> $NGINX_DOCUMENT_ROOT/index.html

# Attendre 5 secondes
sleep 5